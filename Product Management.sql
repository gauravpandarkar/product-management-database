create database product_management1;
use product_management1;

show tables;
CREATE TABLE Product(
Id int,
Name varchar(50),
Quantity varchar(70),
Price varchar(50)
);
drop table Product;
CREATE TABLE Category(
student_id int,
Name varchar(50),
Description varchar(200)
);
CREATE TABLE Variation(
Id int,
Name varchar(50),
Value varchar(60)
);
CREATE TABLE Product_category_map(
product_id int,
Category_id varchar(50)
);
CREATE TABLE Product_Variation_Map(
product_id int,
Variation_id int
);
Insert into Product(Id,Name,Quantity,Price) values (1,'pen','10','20');
Insert into Product(Id,Name,Quantity,Price) values (2,'Book','10','60');
Insert into Product(Id,Name,Quantity,Price) values (3,'Notebook','40','50');
Insert into Product(Id,Name,Quantity,Price) values (4,'Eraser','10','5');
Insert into Product(Id,Name,Quantity,Price) values (5,'Mobile','27','100000');
Insert into Product(Id,Name,Quantity,Price) values (6,'TV','5','200000');
Insert into Product(Id,Name,Quantity,Price) values (7,'Laptop','70','500000');
Insert into Product(Id,Name,Quantity,Price) values (8,'HardDisk','30','800');
Insert into Product(Id,Name,Quantity,Price) values (9,'PenDrive','60','1000');
Insert into Product(Id,Name,Quantity,Price) values (10,'Fridge','6','100000');
Insert into Category(student_id,Name,Description) values (1,'Stationary','All stationary products');
Insert into Category(student_id,Name,Description) values (2,'Home Appliance','All home Appliances');
Insert into Category(student_id,Name,Description) values (3,'Computer','IT satff');
Insert into Category(student_id,Name,Description) values (4,'Electronics','Electronics devices');
insert into Product_category_map (product_id, category_id) values (1 ,1);
insert into Product_category_map (product_id, category_id) values (2 ,1);
insert into Product_category_map (product_id, category_id) values (3 ,1);
insert into Product_category_map (product_id, category_id) values (4 ,1);
insert into Product_category_map (product_id, category_id) values (5 ,2);
insert into Product_category_map (product_id, category_id) values (6 ,2);
insert into Product_category_map (product_id, category_id) values (7 ,2);
insert into Product_category_map (product_id, category_id) values (5 ,4);
insert into Product_category_map (product_id, category_id) values (6 ,4);
insert into Product_category_map (product_id, category_id) values (7 ,4);
insert into Product_category_map (product_id, category_id) values (10 ,2);
insert into Product_category_map (product_id, category_id) values (10 ,4);
insert into Product_category_map (product_id, category_id) values (7 ,3);
insert into Product_category_map (product_id, category_id) values (8 ,3);
insert into Product_category_map (product_id, category_id) values (9 ,3);

insert into Product_variation_Map (product_id, variation_id) values (1 ,1);
insert into Product_variation_Map (product_id, variation_id) values (1 ,2);
insert into Product_variation_Map (product_id, variation_id) values (5 ,1);
insert into Variation (Id, Name, value) values (1 ,'Color', 'Blue,Black,Red');
insert into Variation (Id, Name, value) values (2 ,'Type', 'Marker, ball, Gel');
insert into Variation(Id, Name, value) values (3 ,'Size', '20", 30", 40"');


select Name from Product where Id in (select product_id from Product_category_map where category_id in (select student_id from Category where name="Stationary"));
select Name from Product where Id in (select product_id from Product_Variation_Map where variation_id in (select Id from Variation where name="color"));
select Name from Category where student_id in (select category_id from Product_category_map where category_id in (select Id from Category where Name="Laptop"));
select Name from Variation where Id in (select  variation_id from Product_variation_Map where product_id in (select Id from Variation where Name="Mobile"));
select * from Product;
select * from Category;
select * from Variation;
select * from Product_category_map;
select * from Product_Variation_Map;
update Product set name='abc' where Id=2;
update Product set Name ='dish' where Id = 2;
insert into Product(Id,Name,Quantity,Price) values (11,'notebook','34','34');
insert into Product(Id,Name,Quantity,Price) values (12,'notebook','34','34');




